# Description
Demonstrating Pydantic BaseModel sub-class.  The sub-class has
attributes that are complex datatypes.

The `BusinessSystem` contains two attributes.
* `age` is of type `EPMAttrAgeType`
* `location` is of type `EPMAttrLocationType`

These two attributes are really just an `int` and a `string`, but they will have some 
business specific validation that will be associated with them.

