from typing import Any, Dict, List, Type
from pydantic import BaseModel, GetCoreSchemaHandler
from pydantic_core import core_schema

class DBExecutor:
    pass

class EPMAttrType:
    def epm_validate(self, db: DBExecutor):
        """Validate this specific value with the database"""
        pass

class EPMAttrAgeType(int, EPMAttrType):

    @staticmethod
    def _serialize(value: 'EPMAttrAgeType') -> int:
        return value

    @staticmethod
    def _validate(value: int) -> 'EPMAttrAgeType':
        return EPMAttrAgeType(value)

    @classmethod
    def __get_pydantic_core_schema__(cls,
                                     source: Type[Any],
                                     handler: GetCoreSchemaHandler
                                     ) -> core_schema.CoreSchema:
        # Reference: https://docs.pydantic.dev/2.6/concepts/json_schema/#implementing-__get_pydantic_core_schema__
        return core_schema.no_info_after_validator_function(
            cls._validate,
            core_schema.int_schema(),
            serialization=core_schema.plain_serializer_function_ser_schema(
                cls._serialize,
                info_arg=False,
                return_schema=core_schema.int_schema(),
            ),
        )


    def epm_validate(self, db: DBExecutor):
        """Validate the integer against database to see if the value is valid."""
        if self > 100:
            raise ValueError("Age must be less than 100")

class EPMAttrLocationType(str, EPMAttrType):

    @staticmethod
    def _serialize(value: 'EPMAttrLocationType') -> str:
        return value

    @staticmethod
    def _validate(value: str) -> 'EPMAttrLocationType':
        return EPMAttrLocationType(value)

    @classmethod
    def __get_pydantic_core_schema__(cls,
                                     source: Type[Any],
                                     handler: GetCoreSchemaHandler
                                     ) -> core_schema.CoreSchema:
        # Reference: https://docs.pydantic.dev/2.6/concepts/json_schema/#implementing-__get_pydantic_core_schema__
        return core_schema.no_info_after_validator_function(
            cls._validate,
            core_schema.str_schema(),
            serialization=core_schema.plain_serializer_function_ser_schema(
                cls._serialize,
                info_arg=False,
                return_schema=core_schema.str_schema(),
            ),
        )

    def epm_validate(self, db: DBExecutor):
        """Validate the integer against database to see if the value is valid."""
        if self == 'Florida':
            raise ValueError("Cannot be in Florida")

class EPMBase(BaseModel):
    def epm_validate(self, db: DBExecutor):
        """For all attributes that are based on EPMAttrType, call the epm_validate method"""
        return

    @classmethod
    def is_BaseEPMV_Type(cls, attribute_type: Type):
        """Determine if a declared attribute is inherited from BaseEPMV"""
        return isinstance(attribute_type, type) and issubclass(attribute_type, EPMAttrType)

    @classmethod
    def get_epm_fields(cls):
        """For any sub-class, report the attribute names that inherit from EPMAttrType"""
        # result = []
        # for k,v in cls.model_fields.items():
        #     if cls.is_BaseEPMV_Type(v.annotation):
        #         result.append(k)
        # return result
        return {k: v for k, v in cls.model_fields.items() if cls.is_BaseEPMV_Type(v.annotation)}
