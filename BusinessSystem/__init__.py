from EPMBase import EPMBase, DBExecutor, EPMAttrAgeType, EPMAttrLocationType
from pydantic import Field




class BusinessSystem(EPMBase):
    color: str = Field('beige', description="something that isn't epm")
    age: EPMAttrAgeType = Field(0, description="Age of the thing")
    location: EPMAttrLocationType = Field('Florida', description="the location of the thing.")
    type: str = Field('trivial', description="something that isn't epm. it's trivial.")
