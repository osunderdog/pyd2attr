import pytest
from BusinessSystem import BusinessSystem
from EPMBase import EPMAttrAgeType, EPMAttrLocationType


def test_BusinessSystem_attributes():
    # Wow, Didn't think this was right, but it seems to be
    # https://docs.pydantic.dev/latest/concepts/models/#basic-model-usage
    # The integer is passed in as a string.
    # https://docs.pydantic.dev/latest/concepts/models/#data-conversion
    #
    bs = BusinessSystem(age='10', location='New York')
    assert bs.age == 10
    assert bs.location == 'New York'

    bs = BusinessSystem(age=44, location='Utah')
    assert bs.age == 44
    assert bs.location == 'Utah'

def test_BusinessSystem_complex_attr():
    bs = BusinessSystem(age=EPMAttrAgeType('10'),
                        location=EPMAttrLocationType('New York'))
    assert bs.age == 10
    assert bs.location == 'New York'

def test_BusinessSystem_schema():
    schema = BusinessSystem.model_json_schema()
    assert schema['properties']['age']['type'] == 'integer'
    assert schema['properties']['location']['type'] == 'string'
    print(BusinessSystem.model_json_schema())


def test_BusinessSystem_model_dump():
    bs = BusinessSystem(age=10, location='New York')
    print(bs.model_dump(mode='json'))
    expected_dict = {'age': 10, 'location': 'New York', 'color':'beige', 'type': 'trivial'}
    assert bs.model_dump() == expected_dict