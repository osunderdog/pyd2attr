import pytest
from BusinessSystem import BusinessSystem
from EPMBase import EPMAttrAgeType, EPMAttrLocationType

@pytest.fixture(scope='module')
def business_systems():
    # Wow, Didn't think this was right, but it seems to be
    # https://docs.pydantic.dev/latest/concepts/models/#basic-model-usage
    # The integer is passed in as a string.
    # https://docs.pydantic.dev/latest/concepts/models/#data-conversion
    #
    bslist = [BusinessSystem(age='10', location='New York'), BusinessSystem(age='22', location='Utah')]
    yield bslist



def test_get_epm_attributes(business_systems):
    for bs in business_systems:
        epmattrs = bs.get_epm_fields()
        assert epmattrs == ['age', 'location']